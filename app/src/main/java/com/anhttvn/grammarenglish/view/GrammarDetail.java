package com.anhttvn.grammarenglish.view;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import com.anhttvn.grammarenglish.R;
import com.anhttvn.grammarenglish.base.BaseActivity;
import com.anhttvn.grammarenglish.base.model.News;
import com.anhttvn.grammarenglish.databinding.DetailActivityBinding;
import com.anhttvn.grammarenglish.util.Config;

public class GrammarDetail extends BaseActivity {

  private DetailActivityBinding detailBinding;
  private News news;

  @Override
  public void init() {
    detailBinding.detailContent.setVisibility(View.GONE);
    detailBinding.progress.setVisibility(View.VISIBLE);
    detailBinding.content.requestFocus();
    detailBinding.content.getSettings().setLightTouchEnabled(true);
    detailBinding.content.getSettings().setJavaScriptEnabled(true);
    detailBinding.content.getSettings().setGeolocationEnabled(true);
    detailBinding.content.setSoundEffectsEnabled(true);
    detailBinding.content.setBackgroundColor(0);
    getData();
    isBannerADS(detailBinding.detailAds);
    isBannerADS(detailBinding.adsBottom);
  }

  @Override
  public View contentView() {
    detailBinding = DetailActivityBinding.inflate(getLayoutInflater());
    return detailBinding.getRoot();
  }

  private void getData() {
    Bundle bundle = getIntent().getExtras();
    if (bundle == null) {
      return;
    }
    news = (News) bundle.getSerializable(Config.KEY_DETAIL);
    detailBinding.header.title.setText(Config.formatTitle(news.getType()));
    updateView(news);
  }

  private void updateView(News grammar) {
    if (grammar == null) {
      return;
    }
    if (db.getGrammarLike(grammar.getId(), String.valueOf(1))) {
      detailBinding.like.setImageResource(R.drawable.like_active);
    } else {
      detailBinding.like.setImageResource(R.drawable.like);
    }
    detailBinding.titleDetail.setText(grammar.getTitle());

    detailBinding.content.loadDataWithBaseURL("", grammar.getContent(), "text/html", "UTF-8", "");
    detailBinding.content.setWebChromeClient(new WebChromeClient() {
      @Override
      public void onProgressChanged(WebView view, int newProgress) {
        super.onProgressChanged(view, newProgress);
        if (newProgress == 100) {
          detailBinding.detailContent.setVisibility(View.VISIBLE);
          detailBinding.progress.setVisibility(View.GONE);
        }
      }
    });
  }

  public void back(View view) {
    onBackPressed();
  }
  public void clickLike(View view) {
    if (db.getGrammarLike(news.getId(), String.valueOf(1))) {
      detailBinding.like.setImageResource(R.drawable.like);
      db.updateGrammarLike(news.getId(), 0);
    } else {
      detailBinding.like.setImageResource(R.drawable.like_active);
      db.updateGrammarLike(news.getId(), 1);
    }
  }

  @Override
  public void onBackPressed() {
    isADSFull();
    super.onBackPressed();
  }
}
