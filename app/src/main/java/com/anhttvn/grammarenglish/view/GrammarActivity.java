package com.anhttvn.grammarenglish.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.anhttvn.grammarenglish.base.BaseActivity;
import com.anhttvn.grammarenglish.base.adapter.GrammarAdapter;
import com.anhttvn.grammarenglish.base.api.APIClient;
import com.anhttvn.grammarenglish.base.api.APIInterface;
import com.anhttvn.grammarenglish.base.model.Grammar;
import com.anhttvn.grammarenglish.base.model.Label;
import com.anhttvn.grammarenglish.base.model.News;
import com.anhttvn.grammarenglish.databinding.GrammarActivityBinding;
import com.anhttvn.grammarenglish.util.Config;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GrammarActivity extends BaseActivity implements GrammarAdapter.EventGrammar {
  private GrammarActivityBinding grammarBinding;
  private GrammarAdapter grammarAdapter;
  private boolean isLoading = false;
  private String nextPageToken;
  private Label label;

  private List<News> grammars = new ArrayList<>();
  // API
  private APIInterface apiInterface;
  @Override
  public void init() {
    grammarBinding.data.getRoot().setVisibility(View.GONE);
    getData();
  }

  @Override
  public View contentView() {
    grammarBinding = GrammarActivityBinding.inflate(getLayoutInflater());
    return grammarBinding.getRoot();
  }

  private void getData() {
    Bundle bundle = getIntent().getExtras();
    if (bundle == null) {
      return;
    }
    label = (Label) bundle.getSerializable(Config.KEY_GRAMMAR);
    grammarBinding.header.title.setText(label.getTitle());
    if (isConnected()) {
      loadData(label.getLabel(), null);
    } else {
      grammars = db.grammars(label.getLabel());
      grammarBinding.progressBar.setVisibility(View.GONE);
      adapter(grammars);
    }

  }

  private void loadData(String label, String pageToken) {
    apiInterface = APIClient.getClient().create(APIInterface.class);
    Call<Grammar> newsCall = pageToken != null && pageToken.length() > 0 ?
            apiInterface.getAllNewOfLabel(label, Config.KEY, pageToken) :
            apiInterface.getNewOfLabel("label:"+label, Config.KEY);
    newsCall.enqueue(new Callback<Grammar>() {
      @Override
      public void onResponse(Call<Grammar> call, Response<Grammar> response) {
        if (response.code() == 200) {
          grammarBinding.progressBar.setVisibility(View.GONE);
          if (response.body().getItems() == null || response.body().getItems().size() < 1) {
            grammarBinding.data.getRoot().setVisibility(View.VISIBLE);
            return;
          }
          isLoading = false;
          grammars.addAll(response.body().getItems());
          nextPageToken = response.body().getNextPageToken();
          grammarBinding.progressBar.setVisibility(View.GONE);
          adapter(grammars);
          grammarBinding.news.scrollToPosition(grammars.size() - response.body().getItems().size());

          if (grammars.size() > 0) {
            for (int i = 0; i < grammars.size(); i ++) {
              if (!db.rowExits(grammars.get(i).getId())) {
                grammars.get(i).setType(label);
                grammars.get(i).setLike(0);
                db.updateGrammar(grammars.get(i), 0);
              }
            }
          }
        }
      }

      @Override
      public void onFailure(Call<Grammar> call, Throwable t) {
        grammarBinding.progressBar.setVisibility(View.GONE);
      }
    });
  }

  private void adapter(List<News> grammars) {
    if (grammars != null && grammars.size() > 0) {
      grammarBinding.news.setVisibility(View.VISIBLE);
      grammarAdapter = new GrammarAdapter(this, grammars, this);
      RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
      grammarBinding.news.setLayoutManager(layoutManager);
      grammarBinding.news.setItemAnimator(new DefaultItemAnimator());
      grammarBinding.news.setAdapter(grammarAdapter);
      grammarAdapter.notifyDataSetChanged();
      initScrollListener();
    } else {
      grammarBinding.news.setVisibility(View.GONE);
      grammarBinding.data.getRoot().setVisibility(View.VISIBLE);
    }

  }

  private void initScrollListener() {
    grammarBinding.news.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
      }

      @Override
      public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        if (!isLoading && nextPageToken != null && grammars.size() >= 9) {
          if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == grammars.size() - 1) {
            //bottom of list!
            grammarBinding.progressBar.setVisibility(View.VISIBLE);
            isLoading = true;
            loadData(label.getLabel(), nextPageToken);
          }
        }
      }
    });
  }
  @Override
  public void onClickDetail(int position) {
    Intent intent = new Intent(this, GrammarDetail.class);
    intent.putExtra(Config.KEY_DETAIL, grammars.get(position));
    startActivity(intent);
  }

  public void back(View view) {
    finish();
  }

}
