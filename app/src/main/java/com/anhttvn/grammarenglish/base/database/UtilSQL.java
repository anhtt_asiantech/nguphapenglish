package com.anhttvn.grammarenglish.base.database;

public class UtilSQL {
  public static final String DATABASE_NAME = "GRAMMAR_ENGLISH.db";
  public static final int DATABASE_VERSION = 1;
  public static final String GRAMMAR = "GRAMMAR";
  public static final String LABEL = "LABEL";

  /**
   * information table health & bookmark
   */
  public static final String ID = "ID";
  public static final String TYPE = "TYPE";
  public static final String TITLE = "TITLE";
  public static final String CONTENT = "CONTENT";
  public static final String PUBLISHED = "PUBLISHED";
  public static final String DISPLAY_NAME = "DISPLAY_NAME";
  public static final String LIKED = "LIKED";

  /**
   * information table label
   */

  public static final String URL_PATH ="path";
  /**
   * create table health
   */
  public static final String CREATE_TABLE_GRAMMAR =
          "CREATE TABLE " + GRAMMAR + " (" +
                  ID + " VARCHAR(1000) PRIMARY KEY," +
                  TYPE + " VARCHAR(100)," +
                  TITLE + " TEXT," +
                  CONTENT + " TEXT," +
                  PUBLISHED + " VARCHAR(500)," +
                  DISPLAY_NAME + " VARCHAR(500)," +
                  LIKED + " INTEGER)";

  /**
   * create table label
   */
  public static final String CREATE_TABLE_LABEL =
          "CREATE TABLE " + LABEL + " (" +
                  TYPE + " VARCHAR(100)," +
                  TITLE + " TEXT," +
                  URL_PATH+ " TEXT)";
  /**
   * delete table
   */
  public static final String DELETE_GRAMMAR =
          "DROP TABLE IF EXISTS " + GRAMMAR;
  public static final String DELETE_LABEL =
          "DROP TABLE IF EXISTS " + LABEL;
}
