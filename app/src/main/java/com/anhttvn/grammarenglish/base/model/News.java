package com.anhttvn.grammarenglish.base.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Data;

@Data
public class News implements Serializable {
  @SerializedName("id")
  @Expose
  public String id;
  @SerializedName("published")
  @Expose
  public String published;
  @SerializedName("updated")
  @Expose
  public String updated;
  @SerializedName("url")
  @Expose
  public String url;
  @SerializedName("title")
  @Expose
  public String title;
  @SerializedName("content")
  @Expose
  public String content;
  @SerializedName("author")
  @Expose
  public Author author;

  public String type;

  public int like;


}