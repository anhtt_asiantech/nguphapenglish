package com.anhttvn.grammarenglish.base.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.anhttvn.grammarenglish.R;
import com.anhttvn.grammarenglish.base.model.Label;
import com.squareup.picasso.Picasso;

import java.util.List;

public class LabelAdapter extends RecyclerView.Adapter<LabelAdapter.LabelViewHolder> implements View.OnClickListener {

  private Context context;
  private List<Label> labels;
  private EventLabel eventLabel;

  public LabelAdapter(Context mContext, List<Label> mLabels, EventLabel event) {
    context = mContext;
    labels = mLabels;
    eventLabel = event;
  }

  @NonNull
  @Override
  public LabelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(context)
            .inflate(R.layout.item_label_adapter,parent,false);
    LabelViewHolder itemView = new LabelViewHolder(view);
    return itemView;
  }

  @Override
  public void onBindViewHolder(@NonNull LabelViewHolder holder, int position) {
    Label label = labels.get(position);
    if (label != null) {
      holder.titleLabel.setText(label.getTitle());
      if (label.getImageURL() == null || label.getImageURL().isEmpty()) {
        holder.imageLabel.setImageResource(R.drawable.ic_no_thumbnail);
      } else {
        Picasso.with(context).load(label.getImageURL())
                .placeholder(R.drawable.ic_no_thumbnail)
                .error(R.drawable.ic_no_thumbnail)
                .into(holder.imageLabel);
      }

      holder.cardLabels.setOnClickListener(this);
      holder.cardLabels.setTag(position);

    }
  }

  @Override
  public int getItemCount() {
    return labels.size();
  }

  @Override
  public void onClick(View v) {
    int position = Integer.parseInt(v.getTag()+"");
    switch (v.getId()){
      case R.id.cardLabel:
        eventLabel.clickLabel(position);
        break;

    }
  }

  public class LabelViewHolder extends RecyclerView.ViewHolder {
    protected CardView cardLabels;
    protected ImageView imageLabel;
    protected TextView titleLabel;
    public LabelViewHolder(View view) {
      super(view);
      cardLabels = view.findViewById(R.id.cardLabel);
      imageLabel = view.findViewById(R.id.imgLabel);
      titleLabel = view.findViewById(R.id.title);
    }
  }

  public interface EventLabel {
    void clickLabel(int position);
  }
}
