package com.anhttvn.grammarenglish.base.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class Label implements Serializable {
  public String title;
  public String imageURL;
  public String label;
}
