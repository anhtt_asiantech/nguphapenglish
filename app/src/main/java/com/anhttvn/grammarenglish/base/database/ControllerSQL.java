package com.anhttvn.grammarenglish.base.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.anhttvn.grammarenglish.base.model.Label;
import com.anhttvn.grammarenglish.base.model.News;

import java.util.ArrayList;
import java.util.List;

public class ControllerSQL extends SQLiteOpenHelper {
  public ControllerSQL(Context context) {
    super(context, UtilSQL.DATABASE_NAME, null, UtilSQL.DATABASE_VERSION);
  }
  @Override
  public void onCreate(SQLiteDatabase db) {
    db.execSQL(UtilSQL.CREATE_TABLE_GRAMMAR);
    db.execSQL(UtilSQL.CREATE_TABLE_LABEL);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL(UtilSQL.DELETE_GRAMMAR);
    db.execSQL(UtilSQL.DELETE_LABEL);
    onCreate(db);
  }
  public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    onUpgrade(db, oldVersion, newVersion);
  }

  public void updateGrammar(News news, int like) {
    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues values = new ContentValues();
    values.put(UtilSQL.ID, news.getId());
    values.put(UtilSQL.TYPE, news.getType());
    values.put(UtilSQL.TITLE,news.getTitle());
    values.put(UtilSQL.CONTENT,news.getContent());
    values.put(UtilSQL.PUBLISHED,news.getPublished());
    values.put(UtilSQL.DISPLAY_NAME,news.getAuthor().getDisplayName());
    values.put(UtilSQL.LIKED, like);
    db.insert(UtilSQL.GRAMMAR, null,values);
    db.close();
  }


  public List<News> grammars (String type) {
    List<News> list = new ArrayList<>();
    String query ="SELECT * FROM " +UtilSQL.GRAMMAR +" WHERE " + UtilSQL.TYPE + "='" + type +"'";
    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.rawQuery(query, null);
    cursor.moveToFirst();
    while (!cursor.isAfterLast()) {
      News news = new News();
      news.setId(cursor.getString(0));
      news.setType(cursor.getString(1));
      news.setTitle(cursor.getString(2));
      news.setContent(cursor.getString(3));
      news.setPublished(cursor.getString(4));
      news.setLike(cursor.getInt(6));
      list.add(news);
      cursor.moveToNext();
    }
    cursor.close();
    return list;
  }


  public boolean getGrammarLike(String id, String like) {
    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.query(UtilSQL.GRAMMAR, null,
            UtilSQL.ID + " = ? AND " + UtilSQL.LIKED + " = ?", new String[] { id, like },
            null, null, null);
    cursor.moveToFirst();
    return cursor.getCount() > 0;
  }

  public boolean rowExits(String id) {
    SQLiteDatabase db = this.getWritableDatabase();
    Cursor cursor = db.query(UtilSQL.GRAMMAR, null,
            UtilSQL.ID + " = ?", new String[] { id },
            null, null, null);

    return (cursor.getCount() > 0);
  }

  public void updateLabel(Label label) {
    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues values = new ContentValues();
    values.put(UtilSQL.TITLE,label.getTitle());
    values.put(UtilSQL.TYPE,label.getLabel());
    values.put(UtilSQL.URL_PATH,label.getImageURL());
    db.insert(UtilSQL.LABEL, null,values);
    db.close();
  }

  public List<Label> labels () {
    List<Label> list = new ArrayList<>();
    String query ="SELECT * FROM " +UtilSQL.LABEL;
    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.rawQuery(query, null);
    cursor.moveToFirst();
    while (!cursor.isAfterLast()) {
      Label label = new Label();
      label.setLabel(cursor.getString(0));
      label.setTitle(cursor.getString(1));
      label.setImageURL(cursor.getString(2));
      list.add(label);
      cursor.moveToNext();
    }
    cursor.close();
    return list;
  }

  public boolean isLabelExists(String type) {
    SQLiteDatabase db = this.getWritableDatabase();
    Cursor cursor = db.query(UtilSQL.LABEL, null,
            UtilSQL.TYPE + " = ?", new String[] { type },
            null, null, null);

    return (cursor.getCount() > 0);
  }

  public void updateGrammarLike(String id, int like) {
    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues cv = new ContentValues();
    cv.put(UtilSQL.LIKED, like);
    db.update(UtilSQL.GRAMMAR, cv, UtilSQL.ID + "=?", new String[] {id});
  }


  public List<News> likeGrammars () {
    List<News> list = new ArrayList<>();
    String query ="SELECT * FROM " +UtilSQL.GRAMMAR +" WHERE " + UtilSQL.LIKED + "='" + 1 +"'";
    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.rawQuery(query, null);
    cursor.moveToFirst();
    while (!cursor.isAfterLast()) {
      News news = new News();
      news.setId(cursor.getString(0));
      news.setType(cursor.getString(1));
      news.setTitle(cursor.getString(2));
      news.setContent(cursor.getString(3));
      news.setPublished(cursor.getString(4));
      news.setLike(cursor.getInt(6));
      list.add(news);
      cursor.moveToNext();
    }
    cursor.close();
    return list;
  }
}
