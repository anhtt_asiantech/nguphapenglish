package com.anhttvn.grammarenglish.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.anhttvn.grammarenglish.base.database.ControllerSQL;
import com.anhttvn.grammarenglish.util.Connectivity;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public abstract class BaseFragment extends Fragment {

  protected ControllerSQL db;
  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {

    return initView(inflater,container,false);
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    db = new ControllerSQL(getActivity());
    init();
  }

  protected abstract View initView(LayoutInflater inflater, ViewGroup container, boolean b);
  protected abstract void init();

  protected boolean isConnected() {
    return Connectivity.isConnectedFast(getActivity());
  }

  protected void showAdsBanner (AdView ads) {
    AdRequest adRequest = new AdRequest.Builder()
            .addTestDevice("2C995D2A909C1537C3C52A40B8DA69D9").build();
    if (isConnected()) {
      ads.setVisibility(View.VISIBLE);
      ads.loadAd(adRequest);
    }else{
      ads.setVisibility(View.GONE);
    }
  }

}
