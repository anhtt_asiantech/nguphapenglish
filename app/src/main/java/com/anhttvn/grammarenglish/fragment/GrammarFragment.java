package com.anhttvn.grammarenglish.fragment;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anhttvn.grammarenglish.base.BaseFragment;
import com.anhttvn.grammarenglish.base.adapter.GrammarAdapter;
import com.anhttvn.grammarenglish.base.api.APIClient;
import com.anhttvn.grammarenglish.base.api.APIInterface;
import com.anhttvn.grammarenglish.base.model.Grammar;
import com.anhttvn.grammarenglish.base.model.News;
import com.anhttvn.grammarenglish.databinding.FragmentGrammarBinding;
import com.anhttvn.grammarenglish.util.Config;
import com.anhttvn.grammarenglish.view.GrammarDetail;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GrammarFragment extends BaseFragment implements GrammarAdapter.EventGrammar {
  private String label;
  private FragmentGrammarBinding grammarBinding;
  private GrammarAdapter grammarAdapter;
  private boolean isLoading = false;
  private String nextPageToken;

  private List<News> grammars = new ArrayList<>();
  // API
  private APIInterface apiInterface;

  public GrammarFragment(String title) {
    super();
    label = title;
  }
  @Override
  protected View initView(LayoutInflater inflater, ViewGroup container, boolean b) {
    grammarBinding = FragmentGrammarBinding.inflate(inflater, container, b);
    return grammarBinding.getRoot();
  }

  @Override
  protected void init() {
    grammarBinding.noData.getRoot().setVisibility(View.GONE);
    if (isConnected()) {
      loadData(label, null);
    } else {
      grammars = db.grammars(label);
      grammarBinding.progressBar.setVisibility(View.GONE);
      adapter(grammars);
    }

  }

  private void loadData(String label, String pageToken) {
    if (label == null) {
      return;
    }
    apiInterface = APIClient.getClient().create(APIInterface.class);
    Call<Grammar> newsCall = pageToken != null && pageToken.length() > 0 ?
            apiInterface.getAllNewOfLabel(label, Config.KEY, pageToken) :
            apiInterface.getNewOfLabel("label:"+label, Config.KEY);
    newsCall.enqueue(new Callback<Grammar>() {
      @Override
      public void onResponse(Call<Grammar> call, Response<Grammar> response) {
        if (response.code() == 200) {
          grammarBinding.progressBar.setVisibility(View.GONE);
          if (response.body().getItems() == null || response.body().getItems().size() < 1) {
            grammarBinding.noData.getRoot().setVisibility(View.VISIBLE);
            return;
          }
          isLoading = false;
          grammars.addAll(response.body().getItems());
          nextPageToken = response.body().getNextPageToken();
          grammarBinding.progressBar.setVisibility(View.GONE);
          adapter(grammars);
          grammarBinding.grammars.scrollToPosition(grammars.size() - response.body().getItems().size());

          if (grammars != null && grammars.size() > 0) {
            for (int i = 0; i < grammars.size(); i ++) {
              if (!db.rowExits(grammars.get(i).getId())) {
                grammars.get(i).setType(label);
                grammars.get(i).setLike(0);
                db.updateGrammar(grammars.get(i), 0);
              }
            }
          }
        }
      }

      @Override
      public void onFailure(Call<Grammar> call, Throwable t) {
        grammarBinding.progressBar.setVisibility(View.GONE);
        grammarBinding.grammars.setVisibility(View.GONE);
        grammarBinding.noData.getRoot().setVisibility(View.VISIBLE);
      }
    });
  }

  private void adapter(List<News> grammars) {
    if (grammars != null && grammars.size() > 0) {
      grammarBinding.grammars.setVisibility(View.VISIBLE);
      grammarBinding.noData.getRoot().setVisibility(View.GONE);
      grammarAdapter = new GrammarAdapter(getActivity(), grammars, this);
      RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
      grammarBinding.grammars.setLayoutManager(layoutManager);
      grammarBinding.grammars.setItemAnimator(new DefaultItemAnimator());
      grammarBinding.grammars.setAdapter(grammarAdapter);
      grammarAdapter.notifyDataSetChanged();
      initScrollListener();
    } else {
      grammarBinding.grammars.setVisibility(View.GONE);
      grammarBinding.noData.getRoot().setVisibility(View.VISIBLE);
    }

  }

  private void initScrollListener() {
    grammarBinding.grammars.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
      }

      @Override
      public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        if (!isLoading && nextPageToken != null && grammars.size() >= 9) {
          if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == grammars.size() - 1) {
            //bottom of list!
            grammarBinding.progressBar.setVisibility(View.VISIBLE);
            isLoading = true;
            loadData(label, nextPageToken);
          }
        }
      }
    });
  }

  @Override
  public void onClickDetail(int position) {
    Intent intent = new Intent(getActivity(), GrammarDetail.class);
    grammars.get(position).setType(label);
    intent.putExtra(Config.KEY_DETAIL, grammars.get(position));
    startActivity(intent);
  }
}
