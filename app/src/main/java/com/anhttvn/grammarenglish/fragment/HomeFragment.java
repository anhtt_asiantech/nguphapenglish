package com.anhttvn.grammarenglish.fragment;

import android.content.Intent;
import android.graphics.Paint;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.anhttvn.grammarenglish.R;
import com.anhttvn.grammarenglish.base.BaseFragment;
import com.anhttvn.grammarenglish.base.adapter.LabelAdapter;
import com.anhttvn.grammarenglish.base.adapter.NewAdapter;
import com.anhttvn.grammarenglish.base.api.APIClient;
import com.anhttvn.grammarenglish.base.api.APIInterface;
import com.anhttvn.grammarenglish.base.model.Grammar;
import com.anhttvn.grammarenglish.base.model.Label;
import com.anhttvn.grammarenglish.base.model.News;
import com.anhttvn.grammarenglish.databinding.FragmentHomeBinding;
import com.anhttvn.grammarenglish.util.Config;
import com.anhttvn.grammarenglish.util.Mock;
import com.anhttvn.grammarenglish.view.GrammarActivity;
import com.anhttvn.grammarenglish.view.GrammarDetail;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends BaseFragment implements NewAdapter.NewsEvent, LabelAdapter.EventLabel {

  // API
  private APIInterface apiInterface;
  private DatabaseReference mDatabase;
  // Adapter
  private NewAdapter newAdapter;
  private List<News> grammars = new ArrayList<>();
  private FragmentHomeBinding homeBinding;
  private TextView[] dot;
  private List<Label> labels = new ArrayList<>();
  private LabelAdapter labelAdapter;



  @Override
  protected View initView(LayoutInflater inflater, ViewGroup container, boolean b) {
    homeBinding = FragmentHomeBinding.inflate(inflater, container, b);
    return homeBinding.getRoot();
  }

  @Override
  protected void init() {
    mDatabase = FirebaseDatabase.getInstance().getReference();
    homeBinding.titleNews.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
    homeBinding.titleLabel.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
    homeBinding.noData.getRoot().setVisibility(View.GONE);
    homeBinding.noInternet.getRoot().setVisibility(View.GONE);
    if (isConnected()) {
      loadData(Config.LABEL_TOP_NEW);
      labels = loadLabels();
    } else {
      grammars = db.grammars(Config.LABEL_TOP_NEW);
      labels = db.labels();
      if (grammars != null && grammars.size() > 0) {
        adapter(grammars);
      } else {
        homeBinding.noInternet.getRoot().setVisibility(View.VISIBLE);
      }

      if (labels != null && labels.size() > 0) {
        adapterLabel(labels);
      } else {
        homeBinding.labels.setVisibility(View.GONE);
      }

    }

    showAdsBanner(homeBinding.ads);
  }

  private void adapter(List<News> grammars) {
    if (grammars!= null && grammars.size() > 0) {
      homeBinding.news.setVisibility(View.VISIBLE);
      newAdapter = new NewAdapter(getActivity(), grammars, this);
      homeBinding.news.setAdapter(newAdapter);
      homeBinding.news.setPageMargin(20);
      indicator(grammars,0);
      eventScrollPager(grammars);
    } else {
      homeBinding.news.setVisibility(View.GONE);
      homeBinding.noData.getRoot().setVisibility(View.VISIBLE);
      homeBinding.titleNews.setVisibility(View.GONE);
    }
  }

  private void adapterLabel(List<Label> labels) {
    homeBinding.labels.setVisibility(View.GONE);
    if (labels!= null && labels.size() > 0) {
      homeBinding.labels.setVisibility(View.VISIBLE);
      labelAdapter = new LabelAdapter(getActivity(), labels, this);
      RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL,false);
      homeBinding.labels.setLayoutManager(layoutManager);
      homeBinding.labels.setItemAnimator(new DefaultItemAnimator());
      homeBinding.labels.setAdapter(labelAdapter);
    } else {
      homeBinding.titleLabel.setVisibility(View.GONE);
    }
  }

  private void loadData(String label) {
    apiInterface = APIClient.getClient().create(APIInterface.class);
    Call<Grammar> newsCall = apiInterface.getNewOfLabel("label:"+label, Config.KEY);
    newsCall.enqueue(new Callback<Grammar>() {
      @Override
      public void onResponse(Call<Grammar> call, Response<Grammar> response) {
        if (response.code() == 200) {
          grammars = response.body().getItems();
          adapter(response.body().getItems());
          if (grammars != null && grammars.size() > 0) {
            for (int i = 0; i < grammars.size(); i ++) {
              if (!db.rowExits(grammars.get(i).getId())) {
                grammars.get(i).setType(Config.LABEL_TOP_NEW);
                grammars.get(i).setLike(0);
                db.updateGrammar(grammars.get(i), 0);
              }
            }
          }

        }

      }

      @Override
      public void onFailure(Call<Grammar> call, Throwable t) {
        homeBinding.noData.getRoot().setVisibility(View.VISIBLE);
        homeBinding.news.setVisibility(View.GONE);
        homeBinding.labels.setVisibility(View.GONE);
      }
    });
  }

  private List<Label> loadLabels() {
    List<Label> list = new ArrayList<>();
    DatabaseReference ref2 =  mDatabase.child(Config.LABEL_GRAMMAR);
    ref2.addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot) {
        for (DataSnapshot dsp : dataSnapshot.getChildren()) {
          Label label = dsp.getValue(Label.class);
          if (!db.isLabelExists(label.getLabel())) {
            db.updateLabel(label);
          }
          list.add(label);
        }

        adapterLabel(list);

      }

      @Override
      public void onCancelled(@NonNull DatabaseError error) {
        adapterLabel(labels);
      }
    });

    return list;
  }

  private void indicator(List<News> wallpapers, int position) {
    if (wallpapers.size() < 1) {
      return;
    }
    dot = new TextView[wallpapers.size()];
    homeBinding.dot.removeAllViews();
    for(int i = 0; i < wallpapers.size() ; i++) {
      dot[i] = new TextView(getActivity());
      dot[i].setText(Html.fromHtml("&#9673;"));
      dot[i].setTextSize(10);
      dot[i].setTextColor(getResources().getColor(R.color.darker_gray));
      homeBinding.dot.addView(dot[i]);
    }
    dot[position].setTextColor(getResources().getColor(R.color.red));
  }

  private void eventScrollPager(List<News> grammars) {
    homeBinding.news.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
      @Override
      public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

      }

      @Override
      public void onPageSelected(int position) {
        indicator(grammars, position);
      }

      @Override
      public void onPageScrollStateChanged(int state) {

      }
    });
  }

  @Override
  public void detailNews(int position) {
    Intent intent = new Intent(getActivity(), GrammarDetail.class);
    grammars.get(position).setType(Config.LABEL_TOP_NEW);
    intent.putExtra(Config.KEY_DETAIL, grammars.get(position));
    startActivity(intent);
  }

  @Override
  public void clickLabel(int position) {
    Intent intent = new Intent(getActivity(), GrammarActivity.class);
    intent.putExtra(Config.KEY_GRAMMAR, labels.get(position));
    startActivity(intent);
  }
}
